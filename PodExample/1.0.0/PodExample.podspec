#
# Be sure to run `pod lib lint podexample.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PodExample'
  s.version          = '1.0.0'
  s.summary          = 'Pod example project'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Pod library example for experiment purposes
                       DESC

  s.homepage         = 'https://vloopr.fun'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'randyachmad' => 'randy.blendes17@gmail.com' }
  s.source           = { :git => 'https://oauth2:glpat-GygavL1iYg2TaKHwsrnE@gitlab.com/randyachmad/podexample.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.swift_version = '5.0'
  s.ios.deployment_target = '12.0'
  s.platform = :ios, '12.0'

  s.source_files = 'PodExample/**/*.{h,m,swift}'
#   s.dependency 'Alamofire', '~> 5.4'
#   s.dependency 'PSPDFKit', '~> 10.4.1'
#   s.dependency 'OpenSSL-Universal', '~> 1.1.171'
  s.static_framework = true
  
end
